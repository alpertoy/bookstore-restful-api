<h4>Bookstore Restful API project for registering books in a bookstore via REST architecture</h4>

[![Build Status](https://www.travis-ci.com/alpertoy/bookstore-restful-api.svg?branch=master)](https://www.travis-ci.com/alpertoy/bookstore-restful-api)

[![Quality gate](https://sonarcloud.io/api/project_badges/quality_gate?project=io.gitlab.alpertoy%3Abookstore-restful-api)](https://sonarcloud.io/dashboard?id=io.gitlab.alpertoy%3Abookstore-restful-api)

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/2bd3161ab9669919d973)

<h4>ER Diagram</h4>

![](image/er-diagram.png)

To open swagger documentation (available with Swagger version 3) of all operations implemented with the REST architectural standard, access the following link below:

``
http://localhost:8080/swagger-ui/index.html
``
