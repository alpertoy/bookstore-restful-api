package io.gitlab.alpertoy.bookstorerestfulapi.publishers.builder;

import io.gitlab.alpertoy.bookstorerestfulapi.publishers.dto.PublisherDTO;
import lombok.Builder;

import java.time.LocalDate;

/**
 * User: alpertoy
 * Date: 1.02.2021
 * Time: 00:17
 */

@Builder
public class PublisherDTOBuilder {

    @Builder.Default
    private final Long id = 1L;

    @Builder.Default
    private final String name = "Alper Toy";

    @Builder.Default
    private final String code = "ALP2021";

    @Builder.Default
    private final LocalDate foundationDate = LocalDate.of(2021, 1, 1);

    public PublisherDTO buildPublisherDTO() {
        return new PublisherDTO(id,
                name,
                code,
                foundationDate);
    }
}
