package io.gitlab.alpertoy.bookstorerestfulapi.authors.builder;

import io.gitlab.alpertoy.bookstorerestfulapi.authors.dto.AuthorDTO;
import lombok.Builder;

/**
 * User: alpertoy
 * Date: 31.01.2021
 * Time: 21:21
 */

@Builder
public class AuthorDTOBuilder {

    @Builder.Default
    private final Long id = 1L;

    @Builder.Default
    private final String name = "Alper Toy";

    @Builder.Default
    private final int age = 31;

    public AuthorDTO buildAuthorDTO() {
        return new AuthorDTO(id, name, age);
    }
}
