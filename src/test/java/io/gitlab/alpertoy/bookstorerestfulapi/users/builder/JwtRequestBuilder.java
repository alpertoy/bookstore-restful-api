package io.gitlab.alpertoy.bookstorerestfulapi.users.builder;

import io.gitlab.alpertoy.bookstorerestfulapi.users.dto.JwtRequest;
import lombok.Builder;

/**
 * User: alpertoy
 * Date: 1.02.2021
 * Time: 21:50
 */

@Builder
public class JwtRequestBuilder {

    @Builder.Default
    private String username = "alper";

    @Builder.Default
    private String password = "12345";

    public JwtRequest buildJwtRequest() {
        return new JwtRequest(username, password);
    }
}
