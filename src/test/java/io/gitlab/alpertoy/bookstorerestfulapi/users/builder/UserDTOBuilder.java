package io.gitlab.alpertoy.bookstorerestfulapi.users.builder;

import io.gitlab.alpertoy.bookstorerestfulapi.users.dto.UserDTO;
import io.gitlab.alpertoy.bookstorerestfulapi.users.enums.Gender;
import io.gitlab.alpertoy.bookstorerestfulapi.users.enums.Role;
import lombok.Builder;

import java.time.LocalDate;

/**
 * User: alpertoy
 * Date: 1.02.2021
 * Time: 13:52
 */

@Builder
public class UserDTOBuilder {

    @Builder.Default
    private Long id = 1L;

    @Builder.Default
    private String name = "Alper";

    @Builder.Default
    private int age = 32;

    @Builder.Default
    private Gender gender = Gender.MALE;

    @Builder.Default
    private String email = "alpertoy@gmail.com";

    @Builder.Default
    private String username = "alper";

    @Builder.Default
    private String password = "12345";

    @Builder.Default
    private LocalDate birthdate = LocalDate.of(1989, 9, 16);

    @Builder.Default
    private Role role = Role.USER;

    public UserDTO buildUserDTO() {
        return new UserDTO(id,
                name,
                age,
                gender,
                email,
                username,
                password,
                birthdate,
                role);
    }
}
