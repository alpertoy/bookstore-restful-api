package io.gitlab.alpertoy.bookstorerestfulapi.books.builder;

import io.gitlab.alpertoy.bookstorerestfulapi.books.dto.BookRequestDTO;
import io.gitlab.alpertoy.bookstorerestfulapi.users.builder.UserDTOBuilder;
import io.gitlab.alpertoy.bookstorerestfulapi.users.dto.UserDTO;
import lombok.Builder;

/**
 * User: alpertoy
 * Date: 2.02.2021
 * Time: 00:48
 */

@Builder
public class BookRequestDTOBuilder {

    @Builder.Default
    private final Long id = 1L;

    @Builder.Default
    private final String name = "Spring Boot Pro";

    @Builder.Default
    private final String isbn = "978-3-16-148410-0";

    @Builder.Default
    private final Long publisherId = 2L;

    @Builder.Default
    private final Long authorId = 3L;

    @Builder.Default
    private final Integer pages = 200;

    @Builder.Default
    private final Integer chapters = 10;

    private final UserDTO userDTO = UserDTOBuilder.builder().build().buildUserDTO();

    public BookRequestDTO buildRequestBookDTO() {
        return new BookRequestDTO(id,
                name,
                isbn,
                publisherId,
                authorId,
                pages,
                chapters);
    }
}
