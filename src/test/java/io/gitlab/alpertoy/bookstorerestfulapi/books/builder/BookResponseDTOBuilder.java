package io.gitlab.alpertoy.bookstorerestfulapi.books.builder;

import io.gitlab.alpertoy.bookstorerestfulapi.authors.builder.AuthorDTOBuilder;
import io.gitlab.alpertoy.bookstorerestfulapi.authors.dto.AuthorDTO;
import io.gitlab.alpertoy.bookstorerestfulapi.books.dto.BookResponseDTO;
import io.gitlab.alpertoy.bookstorerestfulapi.publishers.builder.PublisherDTOBuilder;
import io.gitlab.alpertoy.bookstorerestfulapi.publishers.dto.PublisherDTO;
import io.gitlab.alpertoy.bookstorerestfulapi.users.builder.UserDTOBuilder;
import io.gitlab.alpertoy.bookstorerestfulapi.users.dto.UserDTO;
import lombok.Builder;

/**
 * User: alpertoy
 * Date: 2.02.2021
 * Time: 00:51
 */

@Builder
public class BookResponseDTOBuilder {

    @Builder.Default
    private final Long id = 1L;

    @Builder.Default
    private final String name = "Spring Boot Pro";

    @Builder.Default
    private final String isbn = "978-3-16-148410-0";

    @Builder.Default
    private final PublisherDTO publisher = PublisherDTOBuilder.builder().build().buildPublisherDTO();

    @Builder.Default
    private final AuthorDTO author = AuthorDTOBuilder.builder().build().buildAuthorDTO();

    @Builder.Default
    private final Integer pages = 200;

    @Builder.Default
    private final Integer chapters = 10;

    private final UserDTO userDTO = UserDTOBuilder.builder().build().buildUserDTO();

    public BookResponseDTO buildBookResponseDTO() {
        return new BookResponseDTO(id,
                name,
                isbn,
                publisher,
                author,
                pages,
                chapters);
    }
}
