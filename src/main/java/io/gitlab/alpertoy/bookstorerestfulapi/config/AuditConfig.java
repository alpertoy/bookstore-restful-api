package io.gitlab.alpertoy.bookstorerestfulapi.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * User: alpertoy
 * Date: 2.02.2021
 * Time: 02:17
 */

@Configuration
@EnableJpaAuditing(auditorAwareRef = "auditorProvider")
@EnableTransactionManagement
public class AuditConfig {

    @Bean
    public AuditorAware<String> auditorProvider() {
        return new BookstoreAuditorAware();
    }
}
