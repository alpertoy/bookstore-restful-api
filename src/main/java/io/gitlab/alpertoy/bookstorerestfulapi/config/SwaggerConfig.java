package io.gitlab.alpertoy.bookstorerestfulapi.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * User: alpertoy
 * Date: 31.01.2021
 * Time: 18:57
 */

@Configuration
public class SwaggerConfig {

    private static final String BASE_PACKAGE = "io.gitlab.alpertoy.bookstorerestfulapi";
    private static final String API_TITLE = "Bookstore API";
    private static final String API_DESCRIPTION = "Restful API for a bookstore";
    private static final String API_VERSION = "1.0.0";
    private static final String CONTACT_NAME = "Alper Toy";
    private static final String CONTACT_GITLAB = "https://gitlab.com/alpertoy/bookstore-restful-api";
    private static final String CONTACT_EMAIL = "alpertoy@gmail.com";

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage(BASE_PACKAGE))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(buildApiInfo());
    }

    private ApiInfo buildApiInfo() {
        return new ApiInfoBuilder()
                .title(API_TITLE)
                .description(API_DESCRIPTION)
                .version(API_VERSION)
                .contact(new Contact(CONTACT_NAME, CONTACT_GITLAB, CONTACT_EMAIL))
                .build();
    }
}
