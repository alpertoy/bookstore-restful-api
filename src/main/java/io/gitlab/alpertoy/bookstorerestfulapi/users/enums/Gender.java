package io.gitlab.alpertoy.bookstorerestfulapi.users.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * User: alpertoy
 * Date: 31.01.2021
 * Time: 20:07
 */

@Getter
@AllArgsConstructor
public enum Gender {

    MALE("Male"),
    FEMALE("Female");

    private String description;
}
