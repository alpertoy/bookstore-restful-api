package io.gitlab.alpertoy.bookstorerestfulapi.users.service;

import io.gitlab.alpertoy.bookstorerestfulapi.users.dto.MessageDTO;
import io.gitlab.alpertoy.bookstorerestfulapi.users.dto.UserDTO;
import io.gitlab.alpertoy.bookstorerestfulapi.users.entity.User;
import io.gitlab.alpertoy.bookstorerestfulapi.users.exception.UserAlreadyExistsException;
import io.gitlab.alpertoy.bookstorerestfulapi.users.exception.UserNotFoundException;
import io.gitlab.alpertoy.bookstorerestfulapi.users.mapper.UserMapper;
import io.gitlab.alpertoy.bookstorerestfulapi.users.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * User: alpertoy
 * Date: 1.02.2021
 * Time: 13:32
 */

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class UserService {

    private final static UserMapper userMapper = UserMapper.INSTANCE;

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    public MessageDTO create(UserDTO userToCreateDTO) {
        verifyIfExists(userToCreateDTO.getEmail(), userToCreateDTO.getUsername());
        User userToCreate = userMapper.toModel(userToCreateDTO);
        userToCreate.setPassword(passwordEncoder.encode(userToCreate.getPassword()));
        User createdUser = userRepository.save(userToCreate);
        return creationMessage(createdUser);
    }

    public void delete(Long id) {
        verifyAndGetIfExists(id);
        userRepository.deleteById(id);
    }

    public MessageDTO update(Long id, UserDTO userToUpdateDTO) {
        User foundUser = verifyAndGetIfExists(id);

        userToUpdateDTO.setId(id);
        User userToUpdate = userMapper.toModel(userToUpdateDTO);
        userToUpdate.setPassword(passwordEncoder.encode(userToUpdate.getPassword()));
        userToUpdate.setCreatedDate(foundUser.getCreatedDate());
        userToUpdate.setCreatedBy(foundUser.getCreatedBy());

        User updatedUser = userRepository.save(userToUpdate);
        return updateMessage(updatedUser);
    }

    public User verifyAndGetUserIfExists(String username) {
        return userRepository.findByUsername(username)
                .orElseThrow(() -> new UserNotFoundException(username));
    }

    private User verifyAndGetIfExists(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException(id));
    }

    private void verifyIfExists(String email, String username) {
        Optional<User> foundUser = userRepository.findByEmailOrUsername(email, username);
        if (foundUser.isPresent()) {
            throw new UserAlreadyExistsException(email, username);
        }
    }

    private MessageDTO creationMessage(User createdUser) {
        return returnMessage("created", createdUser);
    }

    private MessageDTO updateMessage(User updatedUser) {
        return returnMessage("updated", updatedUser);
    }

    private MessageDTO returnMessage(String action, User user) {
        return MessageDTO.builder()
                .message(String.format("Username %s with ID %s successfully %s", user.getUsername(), user.getId(), action))
                .build();
    }

}
