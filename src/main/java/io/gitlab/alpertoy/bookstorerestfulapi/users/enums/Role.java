package io.gitlab.alpertoy.bookstorerestfulapi.users.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * User: alpertoy
 * Date: 1.02.2021
 * Time: 17:36
 */

@Getter
@AllArgsConstructor
public enum Role {

    ADMIN("ADMIN"),
    USER("USER");

    private final String description;
}
