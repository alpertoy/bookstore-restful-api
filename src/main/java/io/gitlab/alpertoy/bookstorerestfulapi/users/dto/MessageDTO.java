package io.gitlab.alpertoy.bookstorerestfulapi.users.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * User: alpertoy
 * Date: 1.02.2021
 * Time: 14:25
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MessageDTO {

    private String message;
}
