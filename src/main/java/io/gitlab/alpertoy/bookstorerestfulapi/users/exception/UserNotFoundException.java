package io.gitlab.alpertoy.bookstorerestfulapi.users.exception;

import javax.persistence.EntityNotFoundException;

/**
 * User: alpertoy
 * Date: 1.02.2021
 * Time: 15:42
 */
public class UserNotFoundException extends EntityNotFoundException {

    public UserNotFoundException(Long id) {
        super(String.format("User with id %s not exists!", id));
    }

    public UserNotFoundException(String name) {
        super(String.format("User with username %s not exists!", name));
    }
}
