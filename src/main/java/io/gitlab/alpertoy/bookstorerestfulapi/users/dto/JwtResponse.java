package io.gitlab.alpertoy.bookstorerestfulapi.users.dto;

import lombok.Builder;
import lombok.Getter;

/**
 * User: alpertoy
 * Date: 1.02.2021
 * Time: 19:56
 */

@Getter
@Builder
public class JwtResponse {

    private final String jwtToken;
}
