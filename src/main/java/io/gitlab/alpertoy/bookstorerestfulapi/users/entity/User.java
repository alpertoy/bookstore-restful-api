package io.gitlab.alpertoy.bookstorerestfulapi.users.entity;

import io.gitlab.alpertoy.bookstorerestfulapi.books.entity.Book;
import io.gitlab.alpertoy.bookstorerestfulapi.entity.Auditable;
import io.gitlab.alpertoy.bookstorerestfulapi.users.enums.Gender;
import io.gitlab.alpertoy.bookstorerestfulapi.users.enums.Role;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

/**
 * User: alpertoy
 * Date: 31.01.2021
 * Time: 20:07
 */

@Data
@Entity
public class User extends Auditable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private int age;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 20)
    private Gender gender;

    @Column(nullable = false, unique = true, length = 100)
    private String email;

    @Column(nullable = false, unique = true)
    private String username;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false, columnDefinition = "TIMESTAMP")
    private LocalDate birthdate;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private List<Book> books;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 20)
    private Role role;
}
