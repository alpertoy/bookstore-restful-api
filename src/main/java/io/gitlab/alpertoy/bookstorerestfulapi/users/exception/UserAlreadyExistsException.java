package io.gitlab.alpertoy.bookstorerestfulapi.users.exception;

import javax.persistence.EntityExistsException;

/**
 * User: alpertoy
 * Date: 1.02.2021
 * Time: 14:41
 */
public class UserAlreadyExistsException extends EntityExistsException {

    public UserAlreadyExistsException(String email, String username) {
        super(String.format("User with email %s or username %s already exists!", email, username));
    }
}
