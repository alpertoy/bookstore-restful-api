package io.gitlab.alpertoy.bookstorerestfulapi.users.repository;

import io.gitlab.alpertoy.bookstorerestfulapi.users.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * User: alpertoy
 * Date: 31.01.2021
 * Time: 20:28
 */
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByEmailOrUsername(String email, String username);

    Optional<User> findByUsername(String username);
}
