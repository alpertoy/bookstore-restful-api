package io.gitlab.alpertoy.bookstorerestfulapi.users.mapper;

import io.gitlab.alpertoy.bookstorerestfulapi.users.dto.UserDTO;
import io.gitlab.alpertoy.bookstorerestfulapi.users.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * User: alpertoy
 * Date: 1.02.2021
 * Time: 13:13
 */

@Mapper
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    User toModel(UserDTO userDTO);

    UserDTO toDTO(User user);
}
