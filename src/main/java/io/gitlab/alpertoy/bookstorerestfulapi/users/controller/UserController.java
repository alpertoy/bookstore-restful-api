package io.gitlab.alpertoy.bookstorerestfulapi.users.controller;

import io.gitlab.alpertoy.bookstorerestfulapi.users.controller.docs.UserControllerDocs;
import io.gitlab.alpertoy.bookstorerestfulapi.users.dto.JwtRequest;
import io.gitlab.alpertoy.bookstorerestfulapi.users.dto.JwtResponse;
import io.gitlab.alpertoy.bookstorerestfulapi.users.dto.MessageDTO;
import io.gitlab.alpertoy.bookstorerestfulapi.users.dto.UserDTO;
import io.gitlab.alpertoy.bookstorerestfulapi.users.service.AuthenticationService;
import io.gitlab.alpertoy.bookstorerestfulapi.users.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * User: alpertoy
 * Date: 1.02.2021
 * Time: 13:33
 */

@RestController
@RequestMapping("/api/v1/users")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class UserController implements UserControllerDocs {

    private final UserService userService;

    private final AuthenticationService authenticationService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public MessageDTO create(@RequestBody @Valid UserDTO userToSaveDTO) {
        return userService.create(userToSaveDTO);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        userService.delete(id);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public MessageDTO update(@PathVariable Long id, @RequestBody @Valid UserDTO userToUpdateDTO) {
        return userService.update(id, userToUpdateDTO);
    }

    @PostMapping(value = "/authenticate")
    public JwtResponse createAuthenticationToken(@RequestBody @Valid JwtRequest jwtRequest) {
        return authenticationService.createAuthenticationToken(jwtRequest);
    }

}
