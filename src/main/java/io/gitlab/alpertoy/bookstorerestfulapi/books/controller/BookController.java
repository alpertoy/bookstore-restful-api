package io.gitlab.alpertoy.bookstorerestfulapi.books.controller;

import io.gitlab.alpertoy.bookstorerestfulapi.books.controller.docs.BookControllerDocs;
import io.gitlab.alpertoy.bookstorerestfulapi.books.dto.BookRequestDTO;
import io.gitlab.alpertoy.bookstorerestfulapi.books.dto.BookResponseDTO;
import io.gitlab.alpertoy.bookstorerestfulapi.books.service.BookService;
import io.gitlab.alpertoy.bookstorerestfulapi.users.dto.AuthenticatedUser;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * User: alpertoy
 * Date: 2.02.2021
 * Time: 00:35
 */

@RestController
@RequestMapping("/api/v1/books")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class BookController implements BookControllerDocs {

    private final BookService bookService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public BookResponseDTO create(@AuthenticationPrincipal AuthenticatedUser authenticatedUser, @RequestBody @Valid BookRequestDTO bookRequestDTO) {
        return bookService.create(authenticatedUser, bookRequestDTO);
    }

    @GetMapping("/{id}")
    public BookResponseDTO findByIdAndUser(@AuthenticationPrincipal AuthenticatedUser authenticatedUser, @PathVariable Long id) {
        return bookService.findByIdAndUser(authenticatedUser, id);
    }

    @GetMapping
    public List<BookResponseDTO> findAllByUser(@AuthenticationPrincipal AuthenticatedUser authenticatedUser) {
        return bookService.findAllByUser(authenticatedUser);
    }

    @PutMapping("/{id}")
    public BookResponseDTO updateByUser(
            @AuthenticationPrincipal AuthenticatedUser authenticatedUser,
            @PathVariable Long id,
            @RequestBody @Valid BookRequestDTO bookRequestDTO) {
        return bookService.updateByUser(authenticatedUser, id, bookRequestDTO);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteByIdAndUser(
            @AuthenticationPrincipal AuthenticatedUser authenticatedUser,
            @PathVariable Long id) {
        bookService.deleteByIdAndUser(authenticatedUser, id);
    }
}
