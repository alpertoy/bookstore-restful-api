package io.gitlab.alpertoy.bookstorerestfulapi.books.entity;

import io.gitlab.alpertoy.bookstorerestfulapi.authors.entity.Author;
import io.gitlab.alpertoy.bookstorerestfulapi.entity.Auditable;
import io.gitlab.alpertoy.bookstorerestfulapi.publishers.entity.Publisher;
import io.gitlab.alpertoy.bookstorerestfulapi.users.entity.User;
import lombok.Data;

import javax.persistence.*;

/**
 * User: alpertoy
 * Date: 31.01.2021
 * Time: 20:05
 */

@Data
@Entity
public class Book extends Auditable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 100)
    private String name;

    @Column(nullable = false)
    private String isbn;

    @ManyToOne(cascade = {CascadeType.MERGE})
    private Publisher publisher;

    @ManyToOne(cascade = {CascadeType.MERGE})
    private Author author;

    @ManyToOne(cascade = {CascadeType.MERGE})
    private User user;

    @Column(columnDefinition = "integer default 0")
    private int pages;

    @Column(columnDefinition = "integer default 0")
    private int chapters;
}
