package io.gitlab.alpertoy.bookstorerestfulapi.books.mapper;

import io.gitlab.alpertoy.bookstorerestfulapi.books.dto.BookRequestDTO;
import io.gitlab.alpertoy.bookstorerestfulapi.books.dto.BookResponseDTO;
import io.gitlab.alpertoy.bookstorerestfulapi.books.entity.Book;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * User: alpertoy
 * Date: 2.02.2021
 * Time: 00:23
 */

@Mapper
public interface BookMapper {

    BookMapper INSTANCE = Mappers.getMapper(BookMapper.class);

    Book toModel(BookRequestDTO bookRequestDTO);

    Book toModel(BookResponseDTO bookRequestDTO);

    BookResponseDTO toDTO(Book bookDTO);
}
