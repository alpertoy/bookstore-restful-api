package io.gitlab.alpertoy.bookstorerestfulapi.books.dto;

import io.gitlab.alpertoy.bookstorerestfulapi.authors.dto.AuthorDTO;
import io.gitlab.alpertoy.bookstorerestfulapi.publishers.dto.PublisherDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * User: alpertoy
 * Date: 2.02.2021
 * Time: 00:17
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookResponseDTO {

    private Long id;

    private String name;

    private String isbn;

    private PublisherDTO publisher;

    private AuthorDTO author;

    private Integer pages;

    private Integer chapters;
}
