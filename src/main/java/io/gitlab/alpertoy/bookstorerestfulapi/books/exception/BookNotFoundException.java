package io.gitlab.alpertoy.bookstorerestfulapi.books.exception;

import javax.persistence.EntityNotFoundException;

/**
 * User: alpertoy
 * Date: 2.02.2021
 * Time: 02:32
 */
public class BookNotFoundException extends EntityNotFoundException {

    public BookNotFoundException(String name) {
        super(String.format("Book with name %s not exists!", name));
    }

    public BookNotFoundException(Long id) {
        super(String.format("Book with id %s not exists!", id));
    }
}
