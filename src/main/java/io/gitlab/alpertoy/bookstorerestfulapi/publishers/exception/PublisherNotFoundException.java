package io.gitlab.alpertoy.bookstorerestfulapi.publishers.exception;

import javax.persistence.EntityNotFoundException;

/**
 * User: alpertoy
 * Date: 1.02.2021
 * Time: 01:57
 */
public class PublisherNotFoundException extends EntityNotFoundException {

    public PublisherNotFoundException(Long id) {
        super(String.format("Publisher with id %s not exists!", id));
    }
}
