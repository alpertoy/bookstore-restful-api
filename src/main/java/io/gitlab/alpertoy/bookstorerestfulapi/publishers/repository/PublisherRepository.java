package io.gitlab.alpertoy.bookstorerestfulapi.publishers.repository;

import io.gitlab.alpertoy.bookstorerestfulapi.publishers.entity.Publisher;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * User: alpertoy
 * Date: 31.01.2021
 * Time: 20:26
 */
public interface PublisherRepository extends JpaRepository<Publisher, Long> {

    Optional<Publisher> findByNameOrCode(String name, String code);
}
