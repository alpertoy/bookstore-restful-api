package io.gitlab.alpertoy.bookstorerestfulapi.publishers.service;

import io.gitlab.alpertoy.bookstorerestfulapi.publishers.dto.PublisherDTO;
import io.gitlab.alpertoy.bookstorerestfulapi.publishers.entity.Publisher;
import io.gitlab.alpertoy.bookstorerestfulapi.publishers.exception.PublisherAlreadyExistsException;
import io.gitlab.alpertoy.bookstorerestfulapi.publishers.exception.PublisherNotFoundException;
import io.gitlab.alpertoy.bookstorerestfulapi.publishers.mapper.PublisherMapper;
import io.gitlab.alpertoy.bookstorerestfulapi.publishers.repository.PublisherRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * User: alpertoy
 * Date: 1.02.2021
 * Time: 00:09
 */

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class PublisherService {

    private final static PublisherMapper publisherMapper = PublisherMapper.INSTANCE;

    private final PublisherRepository publisherRepository;

    public PublisherDTO create(PublisherDTO publisherDTO) {
        verifyIfExists(publisherDTO.getName(), publisherDTO.getCode());
        Publisher publisherToCreate = publisherMapper.toModel(publisherDTO);
        Publisher createdPublisher = publisherRepository.save(publisherToCreate);
        return publisherMapper.toDTO(createdPublisher);
    }

    public List<PublisherDTO> findAll() {
        return publisherRepository.findAll()
                .stream()
                .map(publisherMapper::toDTO)
                .collect(Collectors.toList());
    }

    public PublisherDTO findById(Long id) {
        return publisherRepository.findById(id)
                .map(publisherMapper::toDTO)
                .orElseThrow(() -> new PublisherNotFoundException(id));
    }

    public void delete(Long id) {
        verifyAndGetIfExists(id);
        publisherRepository.deleteById(id);
    }

    public Publisher verifyAndGetIfExists(Long id) {
        return publisherRepository.findById(id)
                .orElseThrow(() -> new PublisherNotFoundException(id));
    }

    private void verifyIfExists(String name, String code) {
        Optional<Publisher> duplicatedPublisher = publisherRepository.findByNameOrCode(name, code);
        if (duplicatedPublisher.isPresent()) {
            throw new PublisherAlreadyExistsException(name, code);
        }
    }
}
