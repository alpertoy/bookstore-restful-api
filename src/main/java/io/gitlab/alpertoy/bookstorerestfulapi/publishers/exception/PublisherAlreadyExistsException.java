package io.gitlab.alpertoy.bookstorerestfulapi.publishers.exception;

import javax.persistence.EntityExistsException;

/**
 * User: alpertoy
 * Date: 1.02.2021
 * Time: 00:44
 */
public class PublisherAlreadyExistsException extends EntityExistsException {

    public PublisherAlreadyExistsException(String name, String code) {
        super(String.format("Publisher with name %s or code %s already exists!", name, code));
    }

}
