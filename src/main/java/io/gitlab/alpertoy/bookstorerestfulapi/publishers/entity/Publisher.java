package io.gitlab.alpertoy.bookstorerestfulapi.publishers.entity;

import io.gitlab.alpertoy.bookstorerestfulapi.books.entity.Book;
import io.gitlab.alpertoy.bookstorerestfulapi.entity.Auditable;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

/**
 * User: alpertoy
 * Date: 31.01.2021
 * Time: 20:06
 */

@Data
@Entity
public class Publisher extends Auditable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String name;

    @Column(nullable = false, unique = true, length = 100)
    private String code;

    @Column(nullable = false, columnDefinition = "TIMESTAMP")
    private LocalDate foundationDate;

    @OneToMany(mappedBy = "publisher", fetch = FetchType.LAZY)
    private List<Book> books;
}
