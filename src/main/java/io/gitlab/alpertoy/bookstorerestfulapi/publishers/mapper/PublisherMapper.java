package io.gitlab.alpertoy.bookstorerestfulapi.publishers.mapper;

import io.gitlab.alpertoy.bookstorerestfulapi.publishers.dto.PublisherDTO;
import io.gitlab.alpertoy.bookstorerestfulapi.publishers.entity.Publisher;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * User: alpertoy
 * Date: 1.02.2021
 * Time: 00:07
 */

@Mapper
public interface PublisherMapper {

    PublisherMapper INSTANCE = Mappers.getMapper(PublisherMapper.class);

    Publisher toModel(PublisherDTO publisherDTO);

    PublisherDTO toDTO(Publisher publisher);
}
