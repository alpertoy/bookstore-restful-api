package io.gitlab.alpertoy.bookstorerestfulapi.authors.controller;

import io.gitlab.alpertoy.bookstorerestfulapi.authors.controller.docs.AuthorControllerDocs;
import io.gitlab.alpertoy.bookstorerestfulapi.authors.dto.AuthorDTO;
import io.gitlab.alpertoy.bookstorerestfulapi.authors.service.AuthorService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * User: alpertoy
 * Date: 31.01.2021
 * Time: 21:09
 */

@RestController
@RequestMapping("/api/v1/authors")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class AuthorController implements AuthorControllerDocs {

    private final AuthorService authorService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public AuthorDTO create(@RequestBody @Valid AuthorDTO authorDTO) {
        return authorService.create(authorDTO);
    }

    @GetMapping("/{name}")
    public AuthorDTO findByName(@PathVariable String name) {
        return authorService.findByName(name);
    }

    @GetMapping
    public List<AuthorDTO> findAll() {
        return authorService.findAll();
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        authorService.delete(id);
    }
}
