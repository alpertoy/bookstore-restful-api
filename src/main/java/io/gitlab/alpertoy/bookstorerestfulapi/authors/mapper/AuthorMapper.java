package io.gitlab.alpertoy.bookstorerestfulapi.authors.mapper;

import io.gitlab.alpertoy.bookstorerestfulapi.authors.dto.AuthorDTO;
import io.gitlab.alpertoy.bookstorerestfulapi.authors.entity.Author;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * User: alpertoy
 * Date: 31.01.2021
 * Time: 20:59
 */

@Mapper
public interface AuthorMapper {

    AuthorMapper INSTANCE = Mappers.getMapper(AuthorMapper.class);

    Author toModel(AuthorDTO authorDTO);

    AuthorDTO toDTO(Author author);
}
