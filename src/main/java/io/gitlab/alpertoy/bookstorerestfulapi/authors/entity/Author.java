package io.gitlab.alpertoy.bookstorerestfulapi.authors.entity;

import io.gitlab.alpertoy.bookstorerestfulapi.books.entity.Book;
import io.gitlab.alpertoy.bookstorerestfulapi.entity.Auditable;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

/**
 * User: alpertoy
 * Date: 31.01.2021
 * Time: 19:58
 */

@Data
@Entity
public class Author extends Auditable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String name;

    @Column(columnDefinition = "integer default 0")
    private int age;

    @OneToMany(mappedBy = "author", fetch = FetchType.LAZY)
    private List<Book> books;
}
