package io.gitlab.alpertoy.bookstorerestfulapi.authors.exception;

import javax.persistence.EntityNotFoundException;

/**
 * User: alpertoy
 * Date: 31.01.2021
 * Time: 23:06
 */
public class AuthorNotFoundException extends EntityNotFoundException {

    public AuthorNotFoundException(String name) {
        super(String.format("Author with name %s not exists!", name));
    }

    public AuthorNotFoundException(Long id) {
        super(String.format("Author with id %s not exists!", id));
    }
}
