package io.gitlab.alpertoy.bookstorerestfulapi.authors.repository;

import io.gitlab.alpertoy.bookstorerestfulapi.authors.entity.Author;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * User: alpertoy
 * Date: 31.01.2021
 * Time: 20:22
 */

public interface AuthorRepository extends JpaRepository<Author, Long> {

    Optional<Author> findByName(String name);

}
