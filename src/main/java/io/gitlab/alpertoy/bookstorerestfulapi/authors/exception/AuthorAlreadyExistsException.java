package io.gitlab.alpertoy.bookstorerestfulapi.authors.exception;

import javax.persistence.EntityExistsException;

/**
 * User: alpertoy
 * Date: 31.01.2021
 * Time: 21:39
 */

public class AuthorAlreadyExistsException extends EntityExistsException {

    public AuthorAlreadyExistsException(String name, String code) {
        super(String.format("User with name %s or code %code already exists!", name, code));
    }

    public AuthorAlreadyExistsException(String name) {
        super(String.format("User with name %s already exists!", name));
    }
}
