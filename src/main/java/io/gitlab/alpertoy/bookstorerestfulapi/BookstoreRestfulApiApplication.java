package io.gitlab.alpertoy.bookstorerestfulapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookstoreRestfulApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(BookstoreRestfulApiApplication.class, args);
    }

}
